
The apriori.py file takes in two inputs, the first csv should contain the dataset, and the second should contain the itemset

To run the algorithm, in the terminal go to the src directory then enter the following code

python apriori.py "../data/10k_processed_data.csv" "../data/10k_itemlist_data.csv"

