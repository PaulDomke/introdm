import sys, csv


# Template for Assignment 1.
# Author: Haylee Millar, Courtney Miller, Paul Domke
# Date: 9/16/18
# Note: This implementation is not very efficient.
# Hint: @lru_cache(maxsize=None) is likely to be a 
#   favourable decoration for some functions.


# Computes the support of the given itemset in the given database.
# itemset: A set of items
# database: A list of sets of items
# return: The number of sets in the database which itemset is a subset of.
def support(itemset, database):
    count = 0
    for d in database:
        if itemset.issubset(d):
            count = count + 1
    return count


# Computes the confidence of a given rule.
# The rule takes the form precedent --> antecedent
# precedent: A set of items
# antecedent: A set of items that is a superset of precedent
# database: a list of sets of items.
# return: The confidence in precedent --> antecedent.
def confidence(precedent, antecedent, database):
    preSupport = support(precedent, database)
    preAnt = precedent.union(antecedent)
    preAntSupport = support(preAnt, database)
    conf = preAntSupport / preSupport
    return conf


# Finds all itemsets in database that have at least minSupport.
# database: A list of sets of items.
# minSupport: an integer > 1
# return: A list of sets of items, such that
#   s in return --> support(s,database) >= minSupport.
def findFrequentItemsets(itemset, database, minSupport):
    FS = []
    cands = []
    for i in itemset:
        cands.append(i)
    while len(cands) > 0:
        H = []
        for c in cands:
            if support(c, database) >= minSupport and c not in H:
                H.append(c)
                FS.append(c)
        cands = []
        for h in H:
            temp = H
            temp.remove(h)
            for i in temp:
                cands.append(h.union(i))
    return FS


# Given a set of frequently occuring Itemsets, returns
# a list of pairs of the form (precedent, antecedent)
# such that for every returned pair, the rule
# precedent --> antecedent has confidence >= minConfidence
# in the database.
# frequentItemsets: a set or list of sets of items.
# database: A list of sets of items.
# minConfidence: A real value between 0.0 and 1.0.
# return: A set or list of pairs of sets of items.
def findRules(frequentItemsets, database, minConfidence):
    rules = []
    filtered_rules = []
    for i in frequentItemsets:
        for j in frequentItemsets:
            if j != i:
                if i.issubset(j):
                    conf = confidence(i, j, database)
                    print("checking the confidence of a pair")
                    print(conf)
                    if conf >= minConfidence:
                        rules.append((i, j))
                        print("ADDING A RULE")
    for f in rules:
        print("checking out a rule...")
        if f not in filtered_rules:
            filtered_rules.append(f)
            print("adding rule to filtered list")

    return filtered_rules


# Produces a visualization of frequent itemsets.
def visualizeItemsets(frequentItemsets):
    return 0


# Produces a visualization of rules.
def visualizeRules(rules):
    return 0


with open(sys.argv[1], 'r') as csvfile:
    datareader = csv.reader(csvfile)
    database = []
    for row in datareader:
        database.append(row)

# with open(sys.argv[2], 'r') as csvfile:
with open(sys.argv[2], 'r') as f_in:
    lines = list(line for line in (l.strip() for l in f_in) if line)
    itemset = []
    for row in lines:
        word = set()
        word.add(row)
        itemset.append(word)

# Here's a simple test case:

# database = (frozenset([1,2,3]), frozenset([2,3]), frozenset([4,5]), frozenset([1,2]), frozenset([1,5]))
# itemset = [{'1'}, {'2'}, {'3'}, {'4'}, {'5'}]
# itemset = (frozenset([1]), frozenset([2]), frozenset([3]), frozenset([4]), frozenset([5]))
out = findFrequentItemsets(itemset, database, 40)
print(out)  # should print something containing sets {1},{2},{3},{5},{1,2}, and {2,3}.

# Should print something like {2} ===> {1,2}, {1} ===> {1,2}, {3} ===> {2,3}, and {2} === {2,3}
for r in findRules(out, database, 0.85):
    print(str(r[0]) + "  ===>   " + str(r[1]))



        
