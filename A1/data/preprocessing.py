import csv
import string
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
import numpy as np

# list to hold the titles transferred from csv file
titleslist = []

# open csv, add each row in headline_text column to titleslist that has the date 2015.
with open('examiner-date-text.csv', encoding="utf8") as csvfile:
	titlereader = csv.reader(csvfile)
	for row in titlereader:
		if str(row[0][0:4]) == "2015":
			# print(row[0]) 
			# column 1 is only needed
			titleslist.append(row[1])

print("Data copied to list")

#choose randomly 10k titles that were published in 2015.
random10k = np.random.choice(titleslist, 10000, replace=False)

# print(len(titleslist))

# list of stop words according to nltk
stop_words = set(stopwords.words('english'))

# stemmer
st = PorterStemmer()

# list for processed words
processed = []
# item list
itemlist = []

for title in random10k:
	# print statement to ensure code is still running
	print(title)
	# make all letters lowercase
	title = title.lower()
	# remove punctuation
	title = title.translate(str.maketrans('','',string.punctuation))
	# tokenize -- essentially a list of the words in the title
	title = word_tokenize(title)
	# temporary list for processed titles
	temp = []
	for w in title:
		# remove stop words -- done by skipping over them
		if w not in stop_words:
			# remove stemming
			w = st.stem(w)
			if w not in itemlist:
				itemlist.append(w)
			# add to final list of words for the title
			temp.append(w)
	# add to final list of processed titles
	processed.append(temp)

print("Data is processed")

# open process_data.csv to write titles to, encoded utf-8
with open('10k_processed_data.csv', mode='w', encoding='utf-8') as processed_file:
	processed_writer = csv.writer(processed_file, delimiter=',')
	for title in processed:
		# print statement to ensure code is still running
		print(title)
		processed_writer.writerow(title)

print("Data successfully written to 10k_processed_data.csv")

# open itemlist_data.csv to write itemlist to, encoded utf-8
with open('10k_itemlist_data.csv', mode='w', encoding='utf-8') as itemlist_file:
	itemlist_writer = csv.writer(itemlist_file, delimiter=',')
	for w in itemlist:
		# print statement to ensure code is still running
		print(w)
		itemlist_writer.writerow([w])

print("Data successfully writted to 10k_itemlist_data.csv")